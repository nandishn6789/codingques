#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>

// Defines
#define UINT64 uint64_t
// Error codes

#define SUCCESS 0

#define INVALID_FILE 10
#define OOM	20
#define INVALID_RNTRIES 30
#define INTERNAL_ERROR 40

// Represents each line in the CSV file
class FileData {
	public:

		FileData()
			: m_id("")
			, m_parentId("")
			, m_name("")
			, m_isActive("")
		{}
			

		const std::string GetId() const { return m_id ;} 
		const std::string GetParentId() const { return m_parentId; }
		const bool IsActiveRecord() const { return m_isActive; }
		const std::string GetName() const { return m_name;}

    void SetId(std::string val) { m_id = val;}
    void SetParentId(std::string val) { m_parentId = val;}
    void SetName(std::string val) { m_name= val;}
    void SetIsActive(bool val) { m_isActive = val;}
	private:
	// Members
		std::string m_id;
		std::string m_parentId;
		std::string m_name;
		bool m_isActive;
};		


// Structure to represent node in the hierarchy tree
typedef struct hierarchynode
{
	std::string m_id;
	std::string m_name;
	bool m_isActive;
	struct hierarchynode* parentPtr;
	std::vector<struct hierarchynode*> children;
}Node;


// Helper functions for debugging
void PrintFileData(const std::vector<FileData>& fileData)
{
  for (std::vector<FileData>::const_iterator iter = fileData.begin(); iter != fileData.end(); ++iter)
  {
    std::cout << iter->GetId() << ", " << iter->GetName()  << ", " << iter->GetParentId() << ", " << iter->IsActiveRecord() << std::endl;
  }
}

void PrintNameMapping(std::map<std::string, Node*>& mapping)
{
  for (std::map<std::string, Node*>::iterator iter = mapping.begin() ; iter != mapping.end(); ++iter)
  {
    Node* temp = iter->second;    
    std::cout << "Name: " << iter->first << " ID: " << temp->m_id << std::endl;
  }
}

void PrintDictionary(std::map<std::string, std::vector<Node*> >& dict)
{
  for (std::map<std::string, std::vector<Node*> >::iterator iter = dict.begin(); iter != dict.end(); ++iter)
  {
    std::cout << "Name: " << iter->first;
    for (std::vector<Node*>::iterator it = iter->second.begin(); it != iter->second.end(); ++it)
    {
      Node* temp = *it;
      std::cout << " Name: " << temp->m_name << ", ";
    }
    std::cout << std::endl;
  }
}
void PrintHierarchy(Node* root)
{
  if(root == NULL) {
    return;
  }

  std::cout << "RoleName: " << root->m_name << "    ";

  std::cout << " { ";
  for(std::vector<Node*>::iterator iter = root->children.begin(); iter != root->children.end(); ++iter) {
    Node* temp = *iter;
    std::cout << temp->m_name << "," ;
  }
  std::cout << std::endl;

  for(std::vector<Node*>::iterator iter = root->children.begin(); iter != root->children.end(); ++iter) {
    PrintHierarchy(*iter);
  }
}  

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Function to parse the file and file the in memory datastructure
UINT64 ParseCSVFile(std::string fileName,
					std::vector<FileData>& filedata)
{
	std::ifstream file(fileName);
	std::string lineData = "";
	char delimiter = ',';

	while (getline(file, lineData)) {

		int count = 0;
		std::stringstream ss;
		ss.str(lineData);
		std::string word = "";

		std::string id = "", name = "" , parentId= "";
		bool isActive;
		FileData f;
		while (std::getline(ss, word, delimiter)) {
			// Each line should be read as follows
			// role_id, name, isActive, parent_role_id
			switch(count) {
				case 0: f.SetId(word);
						break;
				case 1: f.SetName(word);
						break;
				case 2: f.SetIsActive((word == "true" ? true : false)); 
						break;
				case 3: f.SetParentId(word);
						break;
				default: 
					std::cout << "Invalid file input";
					exit(INVALID_FILE); // This deallocates memory
			}
			count++;
   		}
		filedata.push_back(f);
	}

	file.close();
	return SUCCESS;
}

Node* FormHierarchyTree(const std::vector<FileData>& fileData)
{
  if(fileData.empty())
  {
    return NULL;
  }

  typedef std::map<std::string, std::vector<Node*> > Dictionary;
  typedef std::map<std::string, Node*> NameMapping;

  Dictionary dictionary;
  NameMapping nameMapping;
  Node* root;
               
  for(std::vector<FileData>::const_iterator iter = fileData.begin(); iter != fileData.end(); ++iter)
  {
  
    // Create a node
    Node* tempNode = (Node*)malloc(sizeof(Node));
    if (tempNode == NULL) {
      std::cout << "No memory";
      exit(OOM); // This will de allocate mem too
    }

    tempNode->m_id = iter->GetId();
    tempNode->m_name = iter->GetName();
    tempNode->m_isActive = iter->IsActiveRecord();

    // First insert to nameMapping
    nameMapping[tempNode->m_id] = tempNode;

    // Is this a root?
    if (iter->GetParentId().empty()) {
      root = tempNode;
      // There is no need to add for dictionary
      continue;
    }

    // Check if the parent is present in dictionary. If present, insert this as child
    std::map<std::string, std::vector<Node*> >::iterator dictIter = dictionary.find(iter->GetParentId());
    if (dictIter == dictionary.end()) {
      std::vector<Node*> childList;
      childList.push_back(tempNode);     
      dictionary[iter->GetParentId()] = childList;
    } else {
      dictIter->second.push_back(tempNode);
    }
  }

  // Its time to update the pointers.
  // Loop through dictionary,
  //    1. Attach children to parent
  //    2. Updated child to point to parent

  //PrintNameMapping(nameMapping);
  //PrintDictionary(dictionary);

  for (std::map<std::string, std::vector<Node*> >::iterator iter = dictionary.begin(); iter != dictionary.end(); ++iter)
  {
    // First get the Parent Node
    std::map<std::string, Node*>::iterator parentIter = nameMapping.find(iter->first);
    if (parentIter == nameMapping.end()) {
      std::cout << "Expected to find :" << iter->first;
      exit(INTERNAL_ERROR);
    }

    Node* parent = parentIter->second;

    for (std::vector<Node*>::iterator it = iter->second.begin(); it != iter->second.end(); ++it)
    {                                                                           
      Node* temp = *it;
      parent->children.push_back(temp);
      temp->parentPtr = parent;
    }                                                                           
  } 
  return root; 
}
 
int main(int argc, char* argv[])
{
	if (argc < 2 || argc > 3)
	{
		std::cout << "Invalid number of arguments: filename [considerActiveOnly]";
	}

	// Steps involved
	// 1. Parse the file
	// 2. Validate the data
 	// 3. Build the hierarchy
	// 4. Print the hierarchy
	
 	// Keep track of file data
	std::vector<FileData> fileData;
	
	// Keep track of hierarchy nodes
	Node* root = NULL;
  
  ParseCSVFile(argv[1], fileData); 
  PrintFileData(fileData);

  root = FormHierarchyTree(fileData);
  PrintHierarchy(root);

	return 0;
}	

